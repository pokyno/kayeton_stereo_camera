#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>
#include <ros/package.h>
#include <string>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char **argv){
    ros::init(argc, argv, "kayeton_camera");
    ros::NodeHandle nh;

    //PUBLISHERS
    image_transport::ImageTransport it(nh);
    image_transport::Publisher left_pub = it.advertise("camera/left/image_raw", 1);
    image_transport::Publisher right_pub = it.advertise("camera/right/image_raw", 1);
    
    std::string calibrationFilePath = "file://" + ros::package::getPath("kayeton_stereo_camera") + "/calibration/";
   
    //setting new namespace for each of the cameraManagers
    ros::NodeHandle left_handle("camera/left/");
    ros::NodeHandle right_handle("camera/right/");
    
    //creating all the components for the left camera info pub
    camera_info_manager::CameraInfoManager leftCameraManager(left_handle, "kayeton_left", calibrationFilePath+"kayeton_left.yaml"); 
    sensor_msgs::CameraInfo left_camera_info = leftCameraManager.getCameraInfo();
    ros::Publisher pub_left_camera_info = nh.advertise<sensor_msgs::CameraInfo>("camera/left/camera_info",1);

    //creating all the components for the right camera info pub
    camera_info_manager::CameraInfoManager rightCameraManager(right_handle, "kayeton_right", calibrationFilePath+"kayeton_right.yaml"); 
    sensor_msgs::CameraInfo right_camera_info = rightCameraManager.getCameraInfo();
    ros::Publisher pub_right_camera_info = nh.advertise<sensor_msgs::CameraInfo>("camera/right/camera_info",1);


    //SETTINGS

    //Set the frame rate in Hz
    ros::Rate rate(30); 

    //CAM INIT 
    VideoCapture cap(1);//TODO Change capture input to a variable

    //Check if the connection to the camera is opened succesfully.
    if(!cap.isOpened()){
        ROS_INFO("Cannot open video cam");
    }

    //Get the width and height of the incomming frame
    double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH);
    double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    //The frame contains two sub frames so calculate the width of each sub frame
    double frameWidth = dWidth / 2;


    //ROS SYSTEM
    while(ros::ok()){
        //Read the next frame
        Mat frame;
        bool bSuccess = cap.read(frame);

        //Check if the frame could be retrieved
        if(!bSuccess){
            ROS_INFO("Cannot read from video stream");
            break;
        }

        if(!frame.empty()){
            //Split the frame in the sub frames: leftFrame, rightFrame
            Rect leftFrameCrop(frameWidth, 0, frameWidth, dHeight);
            Rect rightFrameCrop(0, 0, frameWidth, dHeight);

            Mat leftFrame = frame(leftFrameCrop).clone();       
            Mat rightFrame = frame(rightFrameCrop).clone();     

            //Transform from cv::Mat to sensor_msgs::ImagePtr

            std_msgs::Header header = std_msgs::Header();
            sensor_msgs::ImagePtr left_frame_msg = cv_bridge::CvImage(header, "bgr8", leftFrame).toImageMsg();
            sensor_msgs::ImagePtr right_frame_msg = cv_bridge::CvImage(header, "bgr8", rightFrame).toImageMsg();
            

            //Set the header for the camera info
            left_camera_info.header = header;
            right_camera_info.header = header;

            //Publish each sub frame to the correct topic
            pub_left_camera_info.publish(left_camera_info);
            pub_right_camera_info.publish(right_camera_info);

            left_pub.publish(left_frame_msg);
            right_pub.publish(right_frame_msg);     

            waitKey(1);
        }

        ros::spinOnce();
        rate.sleep();
    }   

    cap.release();
}
